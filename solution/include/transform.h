#ifndef IMAGE_TRANSFORMER_TRANSFORM_H
#define IMAGE_TRANSFORMER_TRANSFORM_H

#include "image.h"

/* создаёт копию изображения, которая повёрнута на 90 градусов */
struct image rotate(struct image const *source);

#endif //IMAGE_TRANSFORMER_TRANSFORM_H
