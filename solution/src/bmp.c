#include <stdio.h>
#include <string.h>

#include "bmp.h"
#include "image.h"

enum read_status from_bmp(FILE *in, struct image *img) {
    struct bmp_header header;
    size_t read;

    read = fread(&header, sizeof(header), 1, in);
    if (read != 1 || header.bfType != 0x4D42) {
        return READ_INVALID_SIGNATURE;
    }

    if (header.biBitCount != 24) {
        return READ_INVALID_BITS;
    }

    *img = create_image(header.biWidth, header.biHeight);

    long padding = (long) (4 - img->width * sizeof(struct pixel) % 4) % 4;

    fseek(in, header.bOffBits, SEEK_SET);

    for (int y = 0; y < img->height; ++y) {
        for (int x = 0; x < img->width; ++x) {
            struct pixel *p = get_pixel(img, x, y);
            read = fread(p, sizeof(struct pixel), 1, in);

            if (read != 1) {
                return READ_INVALID_BITS;
            }
        }

        fseek(in, padding, SEEK_CUR);
    }

    return READ_OK;
}

enum write_status to_bmp(FILE *out, struct image *img) {
    struct bmp_header header = {0};
    size_t written;
    unsigned long padding;

    header.bfType = 0x4D42;
    header.bfileSize = sizeof(header) + img->height * ((img->width * sizeof(struct pixel) + 3) & ~3);
    header.bOffBits = sizeof(header);
    header.biSize = 40;
    header.biWidth = img->width;
    header.biHeight = img->height;
    header.biPlanes = 1;
    header.biBitCount = 24;
    header.biCompression = 0;
    header.biSizeImage = 0;
    header.biXPelsPerMeter = 0;
    header.biYPelsPerMeter = 0;
    header.biClrUsed = 0;
    header.biClrImportant = 0;

    padding = (4 - (img->width * sizeof(struct pixel)) % 4) % 4;

    written = fwrite(&header, sizeof(header), 1, out);
    if (written != 1) {
        return WRITE_ERROR;
    }

    for (int y = 0; y < img->height; ++y) {
        for (int x = 0; x < img->width; ++x) {
            struct pixel const *p = get_pixel(img, x, y);
            written = fwrite(p, sizeof(struct pixel), 1, out);
            if (written != 1) {
                return WRITE_ERROR;
            }
        }
        written = fwrite(&padding, 1, padding, out);
        if (written != padding) {
            return WRITE_ERROR;
        }
    }

    return WRITE_OK;
}
