#include <stdlib.h>

#include "image.h"

struct image create_image(uint64_t width, uint64_t height) {
    return (struct image) {
        .width = width,
        .height = height,
        .data = malloc(width * height * sizeof(struct pixel))};
}

void free_image(struct image* img) {
    free(img->data);
}

struct pixel* get_pixel(const struct image* img, uint64_t x, uint64_t y) {
    if (x >= img->width || y >= img->height) {
        return NULL;
    }
    return &img->data[y * img->width + x];
}

void set_pixel(struct image* img, uint64_t x, uint64_t y, struct pixel p) {
    if (x >= img->width || y >= img->height) {
        return;
    }
    img->data[y * img->width + x] = p;
}

