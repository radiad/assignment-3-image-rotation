#include "bmp.h"
#include "transform.h"
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv) {
    (void) argc; (void) argv;
    if (argc != 3) {
        fprintf(stderr, "Usage: %s <source-image> <transformed-image>\n", argv[0]);
        return 1;
    }

    // Открываем файл для чтения
    FILE *input_file = fopen(argv[1], "rb");
    if (input_file == NULL) {
        fprintf(stderr, "Failed to open source image file.\n");
        return 1;
    }

    // Считываем изображение из файла
    struct image source;
    if (from_bmp(input_file, &source) != READ_OK) {
        fprintf(stderr, "Failed to load source image.\n");
        fclose(input_file);
        return 1;
    }

    // Поворачиваем изображение на 90 градусов против часовой стрелки
    struct image rotated = rotate(&source);

    // Открываем файл для записи
    FILE *output_file = fopen(argv[2], "wb");
    if (output_file == NULL) {
        fprintf(stderr, "Failed to open output image file.\n");
        free_image(&source);
        free_image(&rotated);
        return 1;
    }

    // Сохраняем измененное изображение в файл
    if (to_bmp(output_file, &rotated) != WRITE_OK) {
        fprintf(stderr, "Failed to save transformed image.\n");
        fclose(input_file);
        fclose(output_file);
        free_image(&source);
        free_image(&rotated);
        return 1;
    }

    // Освобождаем память, занятую изображениями
    free_image(&source);
    free_image(&rotated);

    // Закрываем файлы
    fclose(input_file);
    fclose(output_file);

    printf("Image transformation complete.\n");
    return 0;
}
