#include "transform.h"
#include "image.h"

/* создаёт копию изображения, которая повёрнута на 90 градусов */
struct image rotate(struct image const *source) {
    uint64_t const height = source->height;
    uint64_t const width = source->width;
    struct image rotated = create_image(height, width);

    for (int y = 0; y < height; ++y) {
        for (uint64_t x = 0; x < width; ++x) {
            /* получаем пиксель из исходного изображения */
            struct pixel const *p = get_pixel(source, x, y);
            /* вычисляем новые координаты для этого пикселя в новом изображении */
            uint64_t new_x = height - y - 1;
            uint64_t new_y = x;
            /* записываем пиксель в новое изображение */
            set_pixel(&rotated, new_x, new_y, *p);
        }
    }

    return rotated;
}
